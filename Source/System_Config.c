#include "System_Config.h"


//片内硬件初始化
void Driver_Init(void)
{
  //Stm32_Clock_Init(9);
  RCC_Config();
  GPIO_Config();
  USART1_Config(115200);
  IIC_Init();
	
}

void RCC_Config(void)
{
  /* Setup the microcontroller system. Initialize the Embedded Flash Interface,  
     initialize the PLL and update the SystemFrequency variable. */
  SystemInit();
  
  /* Enable USART1, GPIOA, GPIOx and AFIO clocks */
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 , ENABLE);
  /* Enable USART2 clock */
}

void GPIO_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC |RCC_APB2Periph_GPIOB , ENABLE); 	
  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;				                 //LED1
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);					 
                                                  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_15  ; 
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
  GPIO_Init(GPIOC, &GPIO_InitStructure);
	
}

void NVIC_Config(void)
{
	
  NVIC_InitTypeDef NVIC_InitStructure;

  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);               //选择中断分组2
  NVIC_InitStructure.NVIC_IRQChannel =TIM2_IRQn;             //选择串口1中断
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;     //抢占式中断优先级设置为0
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;            //响应式中断优先级设置为0
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;               //使能中断
  NVIC_Init(&NVIC_InitStructure);  
  
}

void USART1_Config(u32 baud)
{
  USART_InitTypeDef USART_InitStructure;
  GPIO_InitTypeDef  GPIO_InitStructure; 

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);	


  USART_InitStructure.USART_BaudRate =baud;						                    
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;		               
  USART_InitStructure.USART_StopBits = USART_StopBits_1;			                
  USART_InitStructure.USART_Parity = USART_Parity_No;				                
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;   
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;					

  USART_Init(USART1, &USART_InitStructure);							                
  
  USART_Cmd(USART1, ENABLE);	
}

uint8_t USART_Send(uint8_t us)
{
  USART1->DR = (us );
  while(USART_GetFlagStatus(USART1,USART_FLAG_TC)==RESET);
  return 1;
} 

uint8_t UART_Res(void )
{
  while(!USART_GetFlagStatus(USART1,USART_FLAG_RXNE));
  return (USART_ReceiveData(USART1));
}


////通信协议输出部分
void UART1_ReportIMU(int16_t yaw,int16_t pitch,int16_t roll,int16_t alt,int16_t tempr,int16_t press,int16_t IMUpersec)
{
  unsigned int temp=0xaF+2;
  char ctemp;
  USART_Send(0xa5);
  USART_Send(0x5a);
  USART_Send(14+2);
  USART_Send(0xA1);

  if(yaw<0)
    yaw=32768-yaw;

  ctemp=yaw>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=yaw;
  USART_Send(ctemp);
  temp+=ctemp;

  if(pitch<0)
  pitch=32768-pitch;
  ctemp=pitch>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=pitch;
  USART_Send(ctemp);
  temp+=ctemp;

  if(roll<0)roll=32768-roll;
  ctemp=roll>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=roll;
  USART_Send(ctemp);
  temp+=ctemp;

  if(alt<0)
  alt=32768-alt;
  ctemp=alt>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=alt;
  USART_Send(ctemp);
  temp+=ctemp;

  if(tempr<0)tempr=32768-tempr;
  ctemp=tempr>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=tempr;
  USART_Send(ctemp);
  temp+=ctemp;

  if(press<0)
  press=32768-press;
  ctemp=press>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=press;
  USART_Send(ctemp);
  temp+=ctemp;

  USART_Send(temp%256);
  //putchar(0x09);
  USART_Send(0xaa);
}


void UART1_ReportMotion(int16_t ax,int16_t ay,int16_t az,int16_t gx,int16_t gy,int16_t gz,
					int16_t hx,int16_t hy,int16_t hz)
{
  unsigned int temp=0xaF+9;
  char ctemp;
  USART_Send(0xa5);
  USART_Send(0x5a);
  USART_Send(14+8);
  USART_Send(0xA2);

  if(ax<0)ax=32768-ax;
  ctemp=ax>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=ax;
  USART_Send(ctemp);
  temp+=ctemp;

  if(ay<0)ay=32768-ay;
  ctemp=ay>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=ay;
  USART_Send(ctemp);
  temp+=ctemp;

  if(az<0)az=32768-az;
  ctemp=az>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=az;
  USART_Send(ctemp);
  temp+=ctemp;

  if(gx<0)gx=32768-gx;
  ctemp=gx>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=gx;
  USART_Send(ctemp);
  temp+=ctemp;

  if(gy<0)gy=32768-gy;
  ctemp=gy>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=gy;
  USART_Send(ctemp);
  temp+=ctemp;
//-------------------------
  if(gz<0)gz=32768-gz;
  ctemp=gz>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=gz;
  USART_Send(ctemp);
  temp+=ctemp;

  if(hx<0)hx=32768-hx;
  ctemp=hx>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=hx;
  USART_Send(ctemp);
  temp+=ctemp;

  if(hy<0)hy=32768-hy;
  ctemp=hy>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=hy;
  USART_Send(ctemp);
  temp+=ctemp;

  if(hz<0)hz=32768-hz;
  ctemp=hz>>8;
  USART_Send(ctemp);
  temp+=ctemp;
  ctemp=hz;
  USART_Send(ctemp);
  temp+=ctemp;

  USART_Send(temp%256);
  USART_Send(0xaa);
}


void Timer_Config(void)
{
    
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;   
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  
  TIM_DeInit(TIM2);
  
  
  TIM_TimeBaseInitStructure.TIM_Period=40; // 自动重装载寄存器的值???????????
  TIM_TimeBaseInitStructure.TIM_Prescaler= (36000 - 1); // 时钟预分频数 ?????
  
  
  TIM_TimeBaseInitStructure.TIM_ClockDivision=0x00; // 采样分
  
  TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);
  TIM_ClearFlag(TIM2, TIM_FLAG_Update); // 清除溢出中断标志
  
  TIM2->DIER|=1<<0;   //允许更新中断	
  TIM_Cmd(TIM2, ENABLE); //开启时钟	
}


volatile u32 Time;
void Delay_Us(u32 a)	//示波器显示 1us 偏差较大  10us稍有偏差 其他正常 
{
  Time=a;
  while(Time--);
}
void Delay_Ms(u16 a)	//65535/1000 =65.535 所以 Delay_Us 用u32 
{						//示波器显示1-1000MS 都准确
  Delay_Us(a*1000);
  while(Time--);
}				
				
void Delay(u32 a)
{
  while(a--);
}  




