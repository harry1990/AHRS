#include <acc_comp.h>
#include <math.h>
#include <Matrix.h>

float seq[4];
float wk[3];
float a[3];
float q[4];
float q_back[4];
float qkk[4];
float qk[4];
float af[3];

extern float PP[16];
extern int yuce_only;
float R[3]={1,0.5,0.5};
float Q[4]={0.0001,0.0001,0.0001,0.0001};
void acc_filterUpdate(float* wk,float* qkk,float* a, float* seq)
{ 
int i=0;
float J[12];
float invJ[12];
float seqd[12];
float sita[16];
float invsita[16];
float pz[16];
float pz2[16];
float seqd2[9];
float PinvJ[12];
float KJ[16];
float K[12];
float PP2[16];
float PP2_T[16];
float q0;
float q1;
float q2;
float q3;
float dcm00 ;
float dcm01 ;
float dcm02 ;
float dcm12 ;
float dcm22 ;
float phi_err ;
float theta_err ;
float psi_err ;
float ax;
float ay;
float az;



ax=a[0];
ay=a[1];
az=a[2];

q0=qkk[0];
q1=qkk[1];
q2=qkk[2];
q3=qkk[3];

 dcm00 = 1.0-2*(q2*q2 + q3*q3);
 dcm01 =     2*(q1*q2 + q0*q3);
 dcm02 =     2*(q1*q3 - q0*q2);
 dcm12 =     2*(q2*q3 + q0*q1);
 dcm22 = 1.0-2*(q1*q1 + q2*q2);
 phi_err = 2. / (dcm22*dcm22 + dcm12*dcm12);
 theta_err = -2. / sqrt( 1 - dcm02*dcm02 );
 psi_err = 2. / (dcm00*dcm00 + dcm01*dcm01);
 
 if(ax>3.1415926)
 {
 ax=ax-2*3.1415926;
 }
 else
 {
 ax=ax;
 }
 /*af=Z-Z~*/
af[0]=ax-atan2(2*(q1*q2+q0*q3),q0*q0+q1*q1-q2*q2-q3*q3);
af[1]=ay-asin(-2*(q1*q3-q0*q2));
af[2]=az-atan2(2*(q2*q3+q0*q1),q0*q0-q1*q1-q2*q2+q3*q3);
/*sita为状态矩阵*/
sita[0]=wk[0];
sita[1]=wk[1];
sita[2]=wk[2];
sita[3]=wk[3];

sita[4]=wk[4];
sita[5]=wk[5];
sita[6]=wk[6];
sita[7]=wk[7];

sita[8]=wk[8];
sita[9]=wk[9];
sita[10]=wk[10];
sita[11]=wk[11];

sita[12]=wk[12];
sita[13]=wk[13];
sita[14]=wk[14];
sita[15]=wk[15];
/*J为经过变化后的观测矩阵*/
J[0]= (q3 * dcm00) * psi_err;
J[1]= (q2 * dcm00) * psi_err;
J[2]= (q1 * dcm00 + 2 * q2 * dcm01) * psi_err;
J[3]= (q0 * dcm00 + 2 * q3 * dcm01) * psi_err;

J[4]= -q2 * theta_err;
J[5]=  q3 * theta_err;
J[6]= -q0 * theta_err;
J[7]=  q1 * theta_err;

J[8]= (q1 * dcm22) * phi_err;
J[9]= (q0 * dcm22 + 2 * q1 * dcm12) * phi_err;
J[10]= (q3 * dcm22 + 2 * q2 * dcm12) * phi_err;
J[11]= (q2 * dcm22) * phi_err;

MatrixTranspose(sita,4,4,invsita);
MatrixMultiply(sita,4,4,PP,4,4,pz);
MatrixMultiply(pz,4,4,invsita,4,4,pz2);//PK,K-1





pz2[0]=pz2[0]+Q[0];
pz2[5]=pz2[8]+Q[1];
pz2[10]=pz2[10]+Q[2];
pz2[15]=pz2[15]+Q[3];

MatrixTranspose(J,3,4,invJ);
MatrixMultiply(J,3,4,pz2,4,4,seqd);
MatrixMultiply(seqd,3,4,invJ,4,3,seqd2);//HPH'

//HPH'+R

seqd2[0]=seqd2[0]+R[0];
seqd2[1]=seqd2[1];
seqd2[2]=seqd2[2];

seqd2[3]=seqd2[3];
seqd2[4]=seqd2[4]+R[1];
seqd2[5]=seqd2[5];

seqd2[6]=seqd2[6];
seqd2[7]=seqd2[7];
seqd2[8]=seqd2[8]+R[2];
//(HPH'+R)^-1,PK[9]
MatrixInverse(seqd2,3,0);




MatrixMultiply(pz2,4,4,invJ,4,3,PinvJ);
MatrixMultiply(PinvJ,4,3,seqd2,3,3,K);//K增益阵
//(I-K*J)*pz2,PP
MatrixMultiply(K,4,3,J,3,4,KJ);

for(i=0;i<16;i++)
{
if((i==0)||(i==5)||(i==10)||(i==15))
{
KJ[i]=1-KJ[i];
}
else
{
  KJ[i]=-KJ[i];
}
}



MatrixMultiply(KJ,4,4,pz2,4,4,PP2);
MatrixMultiply(K,4,3,af,3,1,seq);//seq[4],滤波结果
MatrixTranspose(PP2,4,4,PP2_T);

for(i=0;i<16;i++)
{
  PP[i]=PP2[i];
}


}




